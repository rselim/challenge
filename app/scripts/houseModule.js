/**
 * Created by ramyselim on 7/11/15.
 */

/* globals Handlebars*/
/* globals jQuery*/
'use strict';

!function (name,window, definition) {
    if (typeof module !== 'undefined' && module.exports){
        module.exports[name] = definition(jQuery,Handlebars);
    }
    else if (typeof define ==='function' && define.amd){
        define(['jquery','handlebars'],definition);
    }
    else{
        if(typeof(window[name]) === 'undefined'){
            window[name] = definition(jQuery,Handlebars);
        }
        else{
            throw new Error(name + 'namespace is already taken');
        }
    }
}('houseModule',this, function ($,Handlebars) {
        //private data
        var dataArray = [];
        var url = null;

        return {
            getDataArray:function(){
                return dataArray;
            },
            setDataArray:function(data){
                dataArray = data;
            },
            setUrl:function(jsonUrl){
                url = jsonUrl;
            },
            getHousesData:function(){
                var self = this;
                $.get(url).success(function(data){
                    self.setDataArray(data);
                    self.compile();
                });
            },
            compile:function(){
                if(dataArray.length) {
                    var template = $('#box-template').html();
                    var compiledTemplate = Handlebars.compile(template);
                    $('#box-container').append(compiledTemplate(dataArray));

                    this.attachEvents();
                }
            },
            attachEvents:function(){
                $('#box-container').delegate('.btn','click',function(){
                    if($(this).parents('.box').hasClass('active')) {
                        $(this).text('Show more');
                        $(this).parents('.box').removeClass('active');
                    }else{
                        $(this).parents('.box').addClass('active');
                        $(this).text('Show less');
                    }

                    return false;
                });
            },
            clear:function(){
                dataArray = [];
            },
            init:function(jsonUrl){
                this.setUrl(jsonUrl);
                this.getHousesData();
            }
        };
});
